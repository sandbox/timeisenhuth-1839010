A web based command interface.

FEATURES
* Allows any module to implement plugin "Responsers" to respond to commands.
* Plugins may return a success/fail status or Javascript to be evaluated. This allows for running code locally as a return.


FEATURE IDEAS
* Connect to remote sites by specifying an alias and callback URL
* Allow interactive 

